<?php

if($method=='GET') {

    $error = false;
    $ret   = '';
    
    if(isset($path[1]) && isset($path[2])) {

        include 'clases/bbdd.php';
        include 'clases/usuario.php';

        $user = new Usuario();

        $user->login($path[1], md5($path[2]));

        if($user->mail!='') {
            
            $ret = array('error'=>$error, 'mail'=>$user->mail);
        }
        else {
            
            $error = '0002';
        }
    }
    else {

        $error = '0001';
    }
    
    if($error) {
        
        header('Content-Type: application/json');
        echo json_encode(array('error'=>$error));
    }
    else {
        
        header('Content-Type: application/json');
        echo json_encode($ret);
    }
}
else {
    
    header('Content-Type: application/json');
    echo json_encode(array('error'=>'Method not allowed. Use GET.'));
}