<?php

//namespace juanmacordoba\clases;

class Usuario extends Bbdd{
    
	public $id       = '';
	public $mail     = '';
	public $password = '';
	public $fecha    = '';
	public $validado = '';
	public $admin    = '';
	public $error    = '';
	
    /**
     * 
     * @param type $mail
     * @param type $pass
     * @return string
     */
	public function registrar($mail,$pass) {
		
		$mail = addslashes($mail);
		$pass = addslashes($pass);
		
		if (!ereg("^([a-zA-Z0-9._]+)@([a-zA-Z0-9.-]+).([a-zA-Z]{2,4})$",$mail)){ 
			$error = "This mail is not correct, please retype your email.";
		} else {
			
            // conectamos con la bbdd utilizando el m�todo Conectarse de la clase bbdd
            $link = $this->Conectarse();

			// buscamos si ya existe el usuario conectado
			$sql      = "SELECT * FROM `navegalia_usuarios` where mail = '$mail'";
			$result   = mysql_query($sql, $link);
			$num_rows = mysql_num_rows($result);
			
			// cerramos conexion
			mysql_close($link);
			
			// si no hay usuarios con este mail
			if($num_rows==0) {
				// inicializamos id de usuario
				$numero = 0;
				// obtenemos el numero de usuarios de la tabla de usuarios
				$link   = $this->Conectarse();
				$sql    = "SELECT * FROM `navegalia_usuarios`";
				$result = mysql_query($sql, $link);
				if ($row = mysql_fetch_array($result)){
					do{
						$numero = $row["id"];
					} while ($row = mysql_fetch_array($result));
				}
				$numero++;
				mysql_close($link);

				// codificamos el password del usuario para NO guardarlo en claro
				$pass = md5($pass);

				// guardamos el registro en la tabla
				$link   = $this->Conectarse();
				$sql    = " INSERT INTO `navegalia_usuarios` (`id`,`mail`,`password`,`fecha`,`validado`,`admin`) ";
				$sql   .= " VALUES ('$numero','$mail','$pass',curdate(),'no','no' );";
				$result  = mysql_query($sql, $link);
				mysql_close($link);
			
                if($result==false) {
					$error = "Something was wrong with de database, it was not possible to register the user.";
				}
			} else {
				$error = "This mail is alredy been used.";
			}
		}
		
		return $error;
	}
	
    /**
     * 
     * @param type $mail
     * @param type $pass
     * @param string $subject
     * @param type $body
     * @return type
     */
	public function mail($mail,$pass,$subject,$body)
    {
		// codificamos el password del usuario para NO guardarlo en claro
		$pass = md5($pass);
		//define the headers we want passed. Note that they are separated with \r\n
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=utf-8\r\n";
		$headers .= "From: <www.juanmacordoba.com> jm@juanmacordonba.com";
		//define the subject of the email
		$subject = 'juanmacordoba.com - Confirm your mail address'; 
		//define the message to be sent.
		$body  = "Thank you for your registration in www.juanmacordoba.com.\n\r";
		$body .= "Please click the following link as a last step to activate your account:\n\r";
		$body .= "http://www.juanmacordoba.com/user/validate.php?mail=$mail&key=$pass\n\r";
		$body .= "Thank you,\n\r";
		//send the email
		return mail($mail,$subject,$body,$headers);
	}
	
    /**
     * 
     * @param type $mail
     * @return type
     */
	public function borra($mail)
    {
		//
        $mail = addslashes($mail);
		// conectamos con la bbdd utilizando el m�todo Conectarse de la clase bbdd
        $link = $this->Conectarse();
		// borramos al usuario que especifiquemos con mail
		$sql      = "DELETE * FROM `navegalia_usuarios` where mail = '$mail'";
		$result   = mysql_query($sql, $link);
		$num_rows = mysql_num_rows($result);
		// cerramos conexion
		mysql_close($link);
        // devolvemos resultado
		return $result;
	}

    /**
     * 
     * @param type $sql
     */
    public function writeLog($sql='')
    {
        // si la query NO es vacia
        if($sql) {
            // construyo el log
            $content = date("dmYHis")." ".$sql."\n";
            // guardamos contenido generado
            $ar = fopen("public/files/access.log", "a+");
            fputs($ar, $content);
            fclose($ar);
        }
    }
    
    /**
     * 
     * @param type $mail
     * @param type $pass
     * @return boolean
     */
    function login($mail, $pass)
    {
        // inicializamos a NO hay error
		$res = false;
        //
        if($mail && $pass)
        {
            // tratamos las comillas por si nos intentan colar algo...
            $mail = addslashes($mail);
            $pass = addslashes($pass);
            // obtenemos el link con la bbdd
            $handle = $this->getHandle();
            // buscamos si ya existe el usuario conectado
            $sql = "SELECT * FROM `navegalia_usuarios` "
                . "where mail = '$mail' and password = '$pass' and validado = 'si'";
            //
            $res = $handle->query($sql);
            // guardo lo de la query
            $this->writeLog($sql);
            //
            if($res)
            {
                // 
                if ($res->rowCount()==1)
                {
                    //
                    $row = $res->fetch();
                    //
                    if( $mail==$row['mail'] && $pass==$row['password'] )
                    {
                        // SI hay error
                        $res = true;
                        // guardamos las cookies de las credenciales del usuario
                        $this->setCookie('mail', $mail);
                        $this->setCookie('pass', $pass);
                        $this->setCookie('iden', 'si');
                        // guardo en el objeto el mail y el password
                        $this->mail = $mail;
                        $this->password = $pass;
                    }
                    else {
                        $res = false;
                    }
                }
                else {
                    $res = false;
                }
            }
        }
        //
        if(!$res)
        {
			// borramos las cookies de las credenciales del usuario
			$this->delCookie('mail', '');
			$this->delCookie('pass', '');
			$this->delCookie('iden', '');
		}
        // devolvemos resultado
		return $res;
	}
    
    /**
     * Guarda cookie
     * @param type $name
     * @param type $value
     */
    public function setCookie($name, $value)
    {
        // si el nombre de la variable No es vacio
        if($name)
        {
            // guardamos cookie
            setcookie($name, $value, time()+3600, '/', 'www.juanmacordoba.com');
        }
    }
    
    /**
     * Borramos cookie
     * @param type $name
     */
    public function delCookie($name)
    {
        // si el nombre de la variable No es vacio
        if($name)
        {
            // borramos cookie
            setcookie($name, '', time()-3600, '/', 'www.juanmacordoba.com');
        }
    }
    
    /**
     * 
     * @param type $mail
     * @param type $pass
     * @return string
     */
	public function validate($mail,$pass)
    {
		//
		$error = 0;
		//
		$mail = addslashes($mail);
		$pass = addslashes($pass);
		// conectamos con la bbdd utilizando el m�todo Conectarse de la clase bbdd
        $link = $this->Conectarse();
		// buscamos si ya existe el usuario conectado
		$sql      = "SELECT * FROM `navegalia_usuarios` where mail = '$mail' and password = '$pass' and validado = 'no'";
		$result   = mysql_query($sql, $link);
		$num_rows = mysql_num_rows($result);
		// cerramos conexion
		mysql_close($link);
		// si no hay usuarios con este mail
		if($num_rows==1)
        {
			//
            $link   = $this->Conectarse();
			$sql    = "UPDATE `navegalia_usuarios` SET validado = 'si' where mail='$mail' and password='$pass'";
			$result = mysql_query($sql, $link);
			mysql_close($link);
            //
			if($result==false) {
                $error = "2";
            }
		} else {
			$error = "1";
		}
		//
		return $error;
	}
	
    /**
     * 
     * @param type $mail
     * @param type $pass
     * @param type $repass
     * @param type $md5
     * @return string
     */
	public function repass($mail,$pass,$repass,$md5)
    {
		//
		$error = 0;
		//
		$mail = addslashes($mail);
		$pass = addslashes($pass);
		// conectamos con la bbdd utilizando el m�todo Conectarse de la clase bbdd
        $link = $this->Conectarse();
		// buscamos si ya existe el usuario conectado
		$sql      = "SELECT * FROM `navegalia_usuarios` where mail = '$mail' and password = '$md5' and validado = 'si'";
		$result   = mysql_query($sql, $link);
		$num_rows = mysql_num_rows($result);
		// cerramos conexion
		mysql_close($link);
		// si no hay usuarios con este mail
		if($num_rows==1)
        {
			// codificamos el password del usuario para NO guardarlo en claro
			$repass = md5($repass);
			//
			$link   = $this->Conectarse();
			$sql    = "UPDATE `navegalia_usuarios` SET password = '$repass' where mail='$mail' ";
			$result = mysql_query($sql, $link);
			mysql_close($link);
            //
			if($result==false) $error = "2";
		} 
        else {
			//
            $error = "1";
		}
		//
		return $error;
	}
	
    /**
     * 
     * @param type $mail
     * @return string
     */
	public function remember($mail)
    {
		//
		$error = 0;
		$num_rows = 0;
		//
		$mail = addslashes($mail);
		// conectamos con la bbdd utilizando el m�todo Conectarse de la clase bbdd
        $link = $this->Conectarse();
		// buscamos si ya existe el usuario conectado
		$sql      = "SELECT * FROM `navegalia_usuarios` where mail = '$mail' and validado = 'si'";
		$result   = mysql_query($sql, $link);
		//
		//if ($row = mysql_fetch_array($result)){
			do{
				$md5 = $row["password"];
				$num_rows++;
			} while ($row = mysql_fetch_array($result));
		//}
		// cerramos conexion
		mysql_close($link);
		// 
		if($num_rows==1) {
			// define the headers we want passed. Note that they are separated with \r\n
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/plain; charset=utf-8\r\n";
			$headers .= "From: <www.juanmacordoba.com> jm@juanmacordoba.com";
			// define the subject of the email
			$subject = 'juanmacordoba.com - Retype a new password'; 
			// define the message to be sent.
			$body  = "Thank you for using this option in Navegalia.net.\n\r";
			$body .= "Please click the following link to retype a new password for your account:\n\r";
			$body .= "http://www.juanmacordoba.com/user/repass.php?mail=$mail&md5=$md5\n\r";
			$body .= "Thank you,\n\r";
			// send the email
			$result = mail($mail,$subject,$body,$headers);
			//
			if($result==false) {
                //
                $error = "2";
            }
		} else {
			//
            $error = "1";
		}
		//
		return $error;
	}
    
    /**
     * 
     */
    public function disconnect()
    {
        // guardamos las cookies de las credenciales del usuario
        $this->delCookie('mail');
        $this->delCookie('pass');
        $this->delCookie('iden');
        //
        ob_clean();
        header('Location:http://www.juanmacordoba.com/');
        //
        die();
    }
    
    /**
     * Identificamos al usuario por POST o COOKIEs
     * @param type $mailPost
     * @param type $passPost
     * @param type $mailCookie
     * @param type $passCookie
     * @param string $valores
     * @return boolean
     */
    public function identificar($mailPost, $passPost, $mailCookie, $passCookie, &$valores)
    {
        // inicializamos
        $mail = '';
        $pass = '';
        $fin  = true;
        // si las credenciales vienen por POST
        if( $mailPost && $passPost ) {
            // recojemos datos
            $mail = $mailPost;
            $pass = md5($passPost);
        }
        // si las credenciales vienen por COOKIEs
        else if( $mailCookie && $passCookie ) {
            // recojemos datos
            $mail = $mailCookie;
            $pass = $passCookie;
        }
        // si alguno de los campos esta rellenado
        if($mail || $pass) {
            // si el login es correcto
            if($this->login($mail,$pass)) {
                // indicamos que NO es el fin
                $fin = false;
                // indicamos que el usuario esta identificado
                $valores['identificado'] = 'si';
            }
            else {
                // mostramos mensaje de error de login
                $valores['err'] = 'si';
            }
        }
        // devolvemos resultado
        return $fin;
    }
}