<?php

// curl -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:19.0)ecko/20100101 Firefox/19.0" -X GET http://www.juanmacordoba.com/API/clients/$user/$password
// curl -v -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:19.0)ecko/20100101 Firefox/19.0" -X GET http://www.juanmacordoba.com/API/clients/$user/$password

// obtenemos el tipo de llamada de la cabecera HTTP
$method = $_SERVER['REQUEST_METHOD'];

// obtenemos el path de la URL a partir de API/
$path = explode('/', $_SERVER['REDIRECT_QUERY_STRING']);

// dependiendo de la API
if($path[0]=='clients') {
    
    include 'bundle/clients.php';
}
else {
    
    echo 'api not found';
}